from os import environ
from flask import Flask
from flask_restplus import Api
from routes import namespaces
from database.database import db

app = Flask(__name__)

api = Api(app, version='1.0', title='Rent-Store-API',
  description='API para gestão de loja de aluguel' )

app.config['SQLALCHEMY_DATABASE_URI'] = environ.get("DATABASE_ENGINE","sqlite:///local.db")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


for ns in namespaces:
  api.add_namespace(ns)

db.init_app(app)

# primeira execução da aplicação
@app.before_first_request
def create_tables():
  db.create_all()


# execução direta para desenvolvimento
if __name__ == "__main__":
  app.run(host='0.0.0.0',debug=True)
