
from modules.items.resource import api as ns_items
from modules.clients.resource import api as ns_clients
from modules.rentals.resource import api as ns_rentals
from modules.reservations.resource import api as ns_reservations
from modules.item_types.resource import api as ns_item_types

# exporta todos os namespaces registrados em um array, para uso da API

namespaces = [ns_items, ns_clients, ns_rentals, ns_reservations, ns_item_types]