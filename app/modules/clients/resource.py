from flask_restplus import Resource, reqparse, Namespace
from modules.clients.model import ClientModel


api = Namespace('clients', description='Operações relacionadas a gestão de clientes')


@api.route('/<user_name>', endpoint='clients')
@api.doc(params={'user_name': 'Nome de usuário do cliente'})
class Client(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('full_name', type=str, required=True)

    
  def get(self, user_name):
    """
    Retorna o Cliente especificado, caso ele exista
    """

    client = ClientModel.find_by_user_name(user_name)

    if client:
      return client.to_json()
  
    return {'message': 'Cliente não encontrado'}, 404

  

  def delete(self, user_name):
    """
    Exclui um cliente.
    """
    client = ClientModel.find_by_user_name(user_name)

    if client:
      client.delete_from_db()
      return {'message': 'O Cliente foi excluido com sucesso'}, 200
    else:
      return {'message': 'O Cliente a ser excluido não existe'}, 404

  @api.doc(params={'full_name': "Nome completo do cliente"})
  def put(self, user_name):
    """
    Altera informações do cliente, caso exista.
    """

    data = Client.parser.parse_args()

    client = ClientModel.find_by_user_name(user_name)

    if client is None:
       return {"message": "Este Cliente não existe"}, 404

    else:
      
      client.full_name = data['full_name']
      client.save_to_db()

      return client.to_json(), 200


@api.route('/')
class ListAddClient(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('user_name', type=str, required=True)
  parser.add_argument('full_name', type=str, required=True)

  @api.doc(params={'user_name': 'Nome de usuário do cliente', 'full_name': "Nome completo do cliente"})  
  def post(self):
    """
    Cria um cliente, caso ele não exista.
    """

    data = ListAddClient.parser.parse_args()

    user = ClientModel.find_by_user_name(data['user_name'])

    if user:
      return {'message': 'Um cliente com esse usuário já existe'}, 400

    client = ClientModel(data['user_name'], data['full_name'])

    try:
      client.save_to_db()

    except:
      return {"message": "Um Erro ocorreu durante a criação desse cliente"}, 500
    
    return client.to_json(), 201

  def get(self):
    """
    Retorna uma lista com todos os clientes cadastrados.
    """

    all_clients = ClientModel.get_all_records()

    if all_clients:
      all_clients_json = {'clients': [i.to_json() for i in all_clients]}
      return all_clients_json
  
    return {'message': 'Não há clientes cadastrados'}, 404
