from database.database import db

class ClientModel(db.Model):

  __tablename__ = 'clients'

  id = db.Column (db.Integer, primary_key=True)
  user_name =  db.Column(db.String(60), unique=True)
  full_name = db.Column(db.String(80))

  def __init__(self, user_name: str, full_name: str):
    self.user_name = user_name
    self.full_name = full_name

  def __repr__(self): 
    return f"<Cliente(user_name={self.user_name} full_name={self.full_name}>"
  
  def to_json(self):
    return {'id': self.id, 'user_name': self.user_name, 'full_name': self.full_name}

  def save_to_db(self):
    db.session.add(self)
    db.session.commit()

  def delete_from_db(self):
    db.session.delete(self)
    db.session.commit()

  @classmethod
  def find_by_id(cls, id_ : int):
    return cls.query.filter_by(id=id_).first() 

  @classmethod
  def find_by_user_name(cls, user_name : str):
    return cls.query.filter_by(user_name=user_name).first()

  @classmethod
  def get_all_records(cls):
    return cls.query.all()