from flask_restplus import Resource, reqparse, Namespace
from modules.rentals.model import RentalModel
from modules.clients.model import ClientModel
from modules.items.model import ItemModel

from datetime import datetime

api = Namespace('rentals', description='Operações relacionadas ao Aluguel de Itens')


@api.route('/<rental_id>')
class RentalSelect(Resource):

  @api.doc(params={'rental_id': 'ID do aluguel'})
  def get(self, rental_id):
    """
    Retorna a reserva especificado, caso ele exista.
    """

    rental = RentalModel.find_rental_by_id(rental_id)

    if rental:
      return rental.to_json(), 200
  
    return {'message': 'Reserva não encontrada'}, 404




@api.route('/')
class ListRental(Resource):

  def get(self):
    """
    Retorna uma lista com todos os alugueis cadastrados.
    """

    all_items = RentalModel.get_all_records()

    if all_items:
      all_items_json = {'items': [i.to_json() for i in all_items]}
      return all_items_json, 200
  
    return {'message': 'Não há alugueis cadastrados'}, 404


@api.route('/')
class CreateRental(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('user_name', type=str, required=True)
  parser.add_argument('item_id', type=int, required=True)

  @api.doc(params={'user_name': 'usuário do cliente', 'item_id': 'id do item'})
  def post(self):
    """
    Cria um novo aluguel.
    """

    data = CreateRental.parser.parse_args()

    user_name = data['user_name']
    item_id = data['item_id']

    rental = RentalModel.find_active_rental_by_item_id(item_id)
    client = ClientModel.find_by_user_name(user_name)
    item = ItemModel.find_by_id(item_id)

    if rental:
      return {"message": "O Item escolhido já possui um aluguel ativo"}, 500

    if not client:
      return {"message": "O Cliente não existe"}, 500
    
    if not item:
      return {"message": "O Item não existe"}, 500

    rental = RentalModel(user_name=user_name,item_id=item_id)

    try:
      rental.save_to_db()

    except Exception as e:
      return {"message": f"{e} Um Erro ocorreu durante a criação desse aluguel"}, 500
    
    return rental.to_json(), 201


@api.route('/return/<rental_id>')
@api.doc(params={'rental_id': 'id do aluguel'})
class ReturnRental(Resource):
    
  def put(self, rental_id):
    """
    Retorna um item alugado, caso o aluguel exista e esteja ativo.
    """

    rental = RentalModel.find_rental_by_id(rental_id)

    if not rental:
      return {"message": "Este aluguel não existe"}
    if rental.status != "active":
      return {"message": "Este aluguel não está ativo"}
      
    rental.last_updated = datetime.utcnow()
    rental.status = "ended"

    try:
      rental.save_to_db()

    except Exception as e:
      print(e)
      return {"message": "Um Erro ocorreu durante a atualização deste aluguel"}, 500
    
    return rental.to_json(), 201
