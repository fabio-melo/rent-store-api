from flask_restplus import Resource, reqparse, Namespace
from modules.items.model import ItemModel
from modules.item_types.model import ItemTypeModel
api = Namespace('items', description='Operações relacionadas a Items')

@api.route('/<item_id>', endpoint='items')
class Item(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('item_name', type=str, required=True)
  parser.add_argument('item_type', type=str, required=True)

  @api.doc(params={'item_id': 'ID do item'})
  def get(self, item_id):
    """
    Retorna o item especificado, caso ele exista.
    """

    item = ItemModel.find_by_id(item_id)

    if item:
      return item.to_json(), 200
  
    return {'message': 'Item não encontrado'}, 404


  
  @api.doc(params={'item_id': 'ID do item'})
  def delete(self, item_id):
    """
    Exclui o item especificado, caso ele exista.
    """

    item = ItemModel.find_by_id(item_id)
    if item:
      item.delete_from_db()
      return {'message': 'O item foi excluido com sucesso'}, 200
    else:
      return {'message': 'O item a ser excluido não existe'}, 404

  @api.doc(params={'item_id': 'ID do item','item_name': 'nome do Item', 'item_type': 'tipo do item'})
  def put(self, item_id):
    """
    Modifica os dados do item especificado
    """

    data = Item.parser.parse_args()
    item_name = data['item_name']
    item_type = data ['item_type']

    item = ItemModel.find_by_id(item_id)
    type_ = ItemTypeModel.find_by_type(item_type)

    if not item:
       return {"message": "Este Item não existe"}, 403

    if not type_:
      return {"message": "Tipo do item não especificado ou não existe"}, 403


    item.item_name = item_name
    item.item_type = item_type

    item.save_to_db()

    return item.to_json(), 200

@api.route('/')
class ItemCreate(Resource):
  parser = reqparse.RequestParser() 
  
  parser.add_argument('item_name', type=str, required=True)
  parser.add_argument('item_type', type=str, required=True)

  @api.doc(params={'item_name': 'nome do Item', 'item_type': 'tipo do item'})
  def post(self):
    """
    Cadastra um novo Item.
    """

    data = ItemCreate.parser.parse_args()
    item_name = data['item_name']
    item_type = data['item_type']

    type_ = ItemTypeModel.find_by_type(item_type)

    if not type_:
      return {"message": "Tipo do item não especificado ou não existe"}, 403


    item = ItemModel(item_name, item_type)

    try:
      item.save_to_db()

    except:
      return {"message": "Um Erro ocorreu durante a criação desse item"}, 500
    
    return item.to_json(), 201


  def get(self):
    """
    Retorna uma lista com todos os Items cadastrados.
    """

    all_items = ItemModel.get_all_records()

    if all_items:
      all_items_json = {'items': [i.to_json() for i in all_items]}
      return all_items_json, 200
  
    return {'message': 'Não há Items cadastrados'}, 404
