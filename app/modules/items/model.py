from database.database import db

class ItemModel(db.Model):

  __tablename__ = 'items'

  id = db.Column (db.Integer, primary_key=True)
  item_name = db.Column(db.String(60))
  item_type = db.Column(db.String(60))

  def __init__(self, item_name: str, item_type: str):
    self.item_name = item_name
    self.item_type = item_type

  def __repr__(self): 
    return f"<Item(name={self.item_name}, ItemType={self.item_type}>"
  
  def to_json(self):
    return {'id': self.id, 'item_name': self.item_name, 'item_type': self.item_type}

  def save_to_db(self):
    db.session.add(self)
    db.session.commit()

  def delete_from_db(self):
    db.session.delete(self)
    db.session.commit()

  @classmethod
  def find_by_id(cls, id_):
    return cls.query.filter_by(id=id_).first() 

  @classmethod
  def get_all_records(cls):
    return cls.query.all()