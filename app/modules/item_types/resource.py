from flask_restplus import Resource, reqparse, Namespace
from modules.item_types.model import ItemTypeModel

api = Namespace('item_types', description='Operações relacionadas aos Tipos de Items')

@api.route('/<type_id>', endpoint='itemtype')
class ItemType(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('item_type', type=str, required=True)

  def get(self, type_id):
    """
    Retorna o tipo de item especificado, caso ele exista.
    """

    item = ItemTypeModel.find_by_id(type_id)

    if item:
      return item.to_json(), 200
  
    return {'message': 'ItemType não encontrado'}, 404


  
  def delete(self, type_id):
    """
    Exclui o tipo de item especificado, caso ele exista.
    """

    item = ItemTypeModel.find_by_id(type_id)
    if item:
      item.delete_from_db()
      return {'message': 'O tipo de item foi excluido com sucesso'}, 200
    else:
      return {'message': 'O tipo de item a ser excluido não existe'}, 404

  @api.doc(params={'type_id': 'ID do tipo do item','item_type': 'tipo do item'})
  def put(self, type_id):
    """
    Modifica os dados do item especificado
    """

    data = ItemType.parser.parse_args()

    item = ItemTypeModel.find_by_id(type_id)
    print(item)
    if item is None:
       return {"message": "Este Tipo de Item não existe"}, 500

    else:
      
      item.item_type = data['item_type']

      item.save_to_db()

      return item.to_json(), 200

@api.route('/')
class ItemTypeCreate(Resource):
  parser = reqparse.RequestParser() 
  
  parser.add_argument('item_type', type=str, required=True)

  @api.doc(params={'item_type': 'tipo do item'})
  def post(self):
    """
    Cadastra um novo TIpo de Item.
    """

    data = ItemTypeCreate.parser.parse_args()
    
    item_type = data['item_type']

    check = ItemTypeModel.find_by_type(item_type)

    if check:
      return {'message': 'Um tipo de item com esse nome já existe'}, 400

    item_type=  ItemTypeModel(item_type)

    try:
      item_type.save_to_db()

    except:
      return {"message": "Um Erro ocorreu durante a criação desse item"}, 500
    
    return item_type.to_json(), 201


  def get(self):
    """
    Retorna uma lista com todos os tipos de item cadastrados.
    """

    all_items = ItemTypeModel.get_all_records()

    if all_items:
      all_items_json = {'items': [i.to_json() for i in all_items]}
      return all_items_json, 200
  
    return {'message': 'Não há Items cadastrados'}, 404
