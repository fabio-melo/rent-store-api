from database.database import db

class ItemTypeModel(db.Model):

  __tablename__ = 'item_types'

  id = db.Column (db.Integer, primary_key=True)
  item_type = db.Column(db.String(60), nullable=False, unique=True)

  def __init__(self, item_type):
    self.item_type = item_type

  def __repr__(self): 
    return f"<ItemType id={self.id} ItemType={self.item_type}>"
  
  def to_json(self):
    return {'id': self.id, 'item_type': self.item_type}

  def save_to_db(self):
    db.session.add(self)
    db.session.commit()

  def delete_from_db(self):
    db.session.delete(self)
    db.session.commit()

  @classmethod
  def find_by_type(cls, item_type):
    return cls.query.filter_by(item_type=item_type).first() 

  @classmethod
  def find_by_id(cls, id_):
    return cls.query.filter_by(id=id_).first() 
  @classmethod
  def get_all_records(cls):
    return cls.query.all()