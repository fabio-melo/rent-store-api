from database.database import db
from datetime import datetime

from modules.items.model import ItemModel
from modules.clients.model import ClientModel


class ReservationModel(db.Model):
  __tablename__ = 'reservations'

  id = db.Column(db.Integer(), primary_key=True)
  user_name = db.Column(db.String, db.ForeignKey('clients.user_name'))
  item_id = db.Column(db.Integer, db.ForeignKey('items.id'))
  reservation_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
  last_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
  status = db.Column(db.Enum("active","ended","canceled", name="reservation_status_enum"), nullable=False)

  client = db.relationship('ClientModel', backref=db.backref("reservations", cascade="all, delete-orphan" ))
  item = db.relationship('ItemModel', backref=db.backref("reservations", cascade="all, delete-orphan" ))

  def __init__(self, user_name=None, item_id=None, reservation_date=datetime.utcnow()):
    self.user_name = user_name
    self.item_id = item_id
    self.reservation_date = reservation_date
    self.last_updated = datetime.utcnow()
    self.status = "active"


  def __repr__(self): 
    return f"<Rental status={self.status} user_name={self.user_name} item_Id={self.item_id} reservation_date={self.reservation_date} last_updated={self.last_updated}>"
  
  def to_json(self):
    return {'id': self.id, 'user_name': self.user_name, 'item_id': self.item_id, 'status':self.status, 'reservation_date':str(self.reservation_date), 'last_update':str(self.last_updated)}

  def save_to_db(self):
    db.session.add(self)
    db.session.commit()

  def delete_from_db(self):
    db.session.delete(self)
    db.session.commit()

  @classmethod
  def find_reservation_by_id(cls, id_):
    return cls.query.filter_by(id=id_).first() 

  @classmethod
  def find_by_user_name(cls, user_name):
    return cls.query.filter_by(user_name=user_name).all() 

  @classmethod
  def find_by_item_id(cls, id_):
    return cls.query.filter_by(id=id_).first() 

  @classmethod
  def find_active_reservation_by_item_id(cls, item_id):
    return cls.query.filter_by(item_id = item_id, status = "active").first() 

  @classmethod
  def get_all_records(cls):
    return cls.query.all()