from flask_restplus import Resource, reqparse, Namespace
from modules.reservations.model import ReservationModel
from modules.clients.model import ClientModel
from modules.items.model import ItemModel

from datetime import datetime
from dateutil import parser as date_parser

api = Namespace('reservations', description='Operações relacionadas a Reserva de Itens')

@api.route('/<reservation_id>')
class ReservationSelect(Resource):

  @api.doc(params={'reservation_id': 'ID da reserva'})
  def get(self, reservation_id):
    """
    Retorna a reserva especificado, caso ele exista.
    """

    reservation = ReservationModel.find_reservation_by_id(reservation_id)

    if reservation:
      return reservation.to_json()
  
    return {'message': 'Reserva não encontrada'}, 404


@api.route('/')
class ListReservations(Resource):

  def get(self):
    """
    Retorna uma lista com todas as reservas cadastradas.
    """

    all_items = ReservationModel.get_all_records()

    if all_items:
      all_items_json = {'items': [i.to_json() for i in all_items]}
      return all_items_json
  
    return {'message': 'Não há alugueis cadastrados'}, 404


@api.route('/')
class CreateReservation(Resource):
  
  parser = reqparse.RequestParser() 
  
  parser.add_argument('user_name', type=str, required=True)
  parser.add_argument('item_id', type=int, required=True)
  parser.add_argument('reservation_date', type=str, required=True)

  @api.doc(params={'user_name': 'usuário do cliente', 'item_id': 'id do item', 'reservation_date': 'data de reserva no formato "YYYY-MM-DD hh:mm:ss"'})
  def post(self):
    """
    Cria uma nova reserva na data/hora especificada
    """

    data = CreateReservation.parser.parse_args()

    user_name = data['user_name']
    item_id = data['item_id']
    try:
      reservation_date = date_parser.parse(data['reservation_date'])
    except:
      return  {"message": "Formato de data inválido"}, 403
    

    reservation = ReservationModel.find_active_reservation_by_item_id(item_id)
    client = ClientModel.find_by_user_name(user_name)
    item = ItemModel.find_by_id(item_id)

    if reservation:
      return {"message": "O Item escolhido já possui uma Reserva ativa"}, 403
    
    if not client:
      return {"message": "O Cliente não existe"}, 403
    
    if not item:
      return {"message": "O Item não existe"}, 403
    #TODO DATAS
    reservation = ReservationModel(user_name=user_name,item_id=item_id, reservation_date=reservation_date)

    try:
      reservation.save_to_db()

    except Exception as e:
      print(e)
      return {"message": "Um Erro ocorreu durante a criação desse aluguel"}, 500
    
    return reservation.to_json(), 201


@api.route('/cancel/<reservation_id>')
@api.doc(params={'reservation_id': 'id da reserva'})
class ReturnReservation(Resource):
    
  def put(self, reservation_id):
    """
    Cancela uma reserva, caso a reserva exista e esteja ativa.
    """

    reservation = ReservationModel.find_reservation_by_id(reservation_id)

    if not reservation:
      return {"message": "Esta reserva não existe"}
    if reservation.status != "active":
      return {"message": "Esta reserva não está ativa"}
      
    reservation.last_updated = datetime.utcnow()
    reservation.status = "ended"

    try:
      reservation.save_to_db()

    except Exception as e:
      return {"message": "Um Erro ocorreu durante a atualização desta reserva"}, 500
    
    return reservation.to_json(), 201
