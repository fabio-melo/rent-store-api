import csv
import os
import requests
from tqdm import tqdm, trange
import random
from datetime import datetime, timedelta

# capturar o caminho relativo
path = os.path.dirname(os.path.realpath(__file__))


# gerar dados e cadastrar

def import_test_data():

  with open(os.path.join(path,"mock_item_types.csv"),'r') as file:
    reader = csv.DictReader(file)
    item_types = [{'item_type': x['item_type']} for x in reader]
  with open(os.path.join(path,"mock_items.csv"),'r') as file:
    reader = csv.DictReader(file)
    items = [{'item_name':x['item_name'],'item_type': x['item_type']} for x in reader]

  with open(os.path.join(path,"mock_users.csv"),'r') as file:
    reader = csv.DictReader(file)
    clients = [{'user_name':x['user_name'],'full_name': x['full_name']} for x in reader]

  # requisitar ao banco
  for x in tqdm(item_types, "Cadastrando Tipos de Items"):
    requests.post('http://localhost:8010/item_types', data=x)
  
  for x in tqdm(items, "Cadastrando Items"):
    requests.post('http://localhost:8010/items', data=x)

  for x in tqdm(clients, "Cadastrando Clientes", total=500):
    requests.post('http://localhost:8010/clients', data=x)
      
  return item_types, items, clients


if __name__ == '__main__':

  print("Executando Suite de Testes")
  item_types, items, clients = import_test_data()

  for x in tqdm(range(100), "Executando Reservas Aleatórias"):
    client = random.choice(clients)
    data = {'user_name': client['user_name'], 'item_id': random.randint(1,1000), 'reservation_date':datetime.now() + timedelta(days=random.randint(5,100)) }
    r = requests.post('http://localhost:8010/reservations', data=data)

  for x in tqdm(range(100), "Executando Alugueis Aleatórios"):
    client = random.choice(clients)
    data = {'user_name': client['user_name'], 'item_id': random.randint(1,1000) }
    r = requests.post('http://localhost:8010/rentals', data=data)
    print(r)

  for x in tqdm(range(20), "Executando Devoluções Aleatórias"):
    data = { 'rental_id': random.randint(1,100) }
    r = requests.put('http://localhost:8010/rentals/return/', params=data)

  for x in tqdm(range(20), "Executando Cancelamentos Aleatórias"):
    data = { 'rental_id': random.randint(1,100) }
    r = requests.put('http://localhost:8010/cancel/return/', params=data)
