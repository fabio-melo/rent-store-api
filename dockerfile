FROM python:3.8.1-slim-buster

WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt


CMD ["gunicorn", "app.main:app", "-b", "0.0.0.0:8010"]
