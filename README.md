# rent-store-api
API RESTful para Gerenciamento de uma Loja de Aluguel

-----------------

## Funcionalidades

* Implementação de Rotas de Gerência do sistema loja de aluguel.
* Persistência em Banco de Dados PostgreSQL.
* Documentação de API através de Swagger/OpenAPI.
* Encapsulamento e Modularização de Recursos.
* Testes de Persistência de Funcionalidades.

-----------------

## Documentação

* Para acessar a documentação do projeto basta abrir a URL do servidor da api no navegador.

  ex: localhost:8080

-----------------

## Tecnologias Utilizadas

<b>Desenvolvido com</b>
- Python 3.6+ (Flask, SQLAlchemy, Gunicorn)
- PostgreSQL 9.6
- Swagger UI


-----------------


## Estrutura do Projeto

    app
    │   main.py                  # ponto de entrada da API
    │   routes.py                # definição de rotas e namespaces
    │
    ├───database                   
    │   │   database.py          # chamada de persistência de banco de dados
    ├───modules
    │   ├───clients              # módulo de gerência de clientes
    │   │   │   model.py         # modelo de cliente
    │   │   │   resource.py      # recursos de rotas
    │   ├───items                # módulo de gerência de itens
    │   │   │   model.py
    │   │   │   resource.py
    │   ├───item_types           # módulo de gerência de tipo de item
    │   │   │   model.py
    │   │   │   resource.py
    │   ├───rentals              # módulo de gerência de alugueis
    │   │   │   model.py
    │   │   │   resource.py
    │   └───reservations         # módulo de gerência de reservas
    │       │   model.py
    │       │   resource.py

    tests                        # testes de persistência
    │  mock_builder.py           # script de testes de bd
    │  mock_items.csv            # tabela mock de itens
    │  mock_item_types.csv       # tabela mock de tipos de itens
    │  mock_users.csv            # tabela mock de usuários

---------------------

## Instalação via docker-compose (Recomendado)

Para instalação via docker-compose, com PostgreSQL e dependências inclusas, é apenas necessário a execução do seguinte comando:

    docker-compose up

-----------------

## Instalação via dockerfile

Para instalação via dockerfile, é necessário a execução do seguinte comando:

      docker build .
      docker start

-------------------

## Instalação Local

Para executar a aplicação é necessário Python 3.6 ou superior, e a instalação dos módulos requisitados.

      pip install -r requirements.txt

Para executar a aplicação em modo de desenvolvimento pode se iniciar o servidor diretamente utilizando o comando:

      python app.py

Para execução em modo de produção é necessário um serviço de produção WSGI, como o gunicorn, em ambiente linux:

      gunicorn --workers=4 app.main:app

-------------------

## Variáveis de Ambiente

O Projeto utiliza-se de variáveis de ambiente, presentes em *app.env*, e são elas:

    - DATABASE_ENGINE= # url e definição do motor do banco de dados

Para configuração da imagem do PostgreSQL, deverão ser setados


    POSTGRES_USER=user
    POSTGRES_PASSWORD=password
    POSTGRES_DB=database

--------------------

## Testes e Mock de Banco de Dados
Para executar um teste de preenchimento e mock da estrutura do banco, pode se utilizar o script *mock_builder.py*, disponível na pasta *test*.
Este script irá popular todas as tabelas da aplicação com dados de teste.

----------------------

----------------------